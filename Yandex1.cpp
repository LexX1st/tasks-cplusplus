#include <iostream>
#include <string>
#include <set>

using namespace std;

int main(){
    string First, Second;
    cin >> First;
    cin >> Second;
    
    set<char> jewil;
    for(char i: First){
        jewil.insert(i);
    }
    int Count = 0;
    for(char i: Second){
        if(jewil.count(i)){
            Count++;
        }
    }
    cout << Count;
    return 0;
}