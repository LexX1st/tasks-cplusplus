#include <iostream>
using namespace std;

int main() {
    string menu[] = {"fruits", "chicken", "fish", "cake"};
    try {
        int x;
        cin >> x;
        //your code goes here
        cout << menu[x];
    }
    catch(...) {
        //and here
        cout << "404 - not found";
    }
}