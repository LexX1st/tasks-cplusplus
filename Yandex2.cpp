#include <iostream>

using namespace std;

int main(){
    int dimSize;
    cin >> dimSize;
    int rating[dimSize];
    const int Min = 0;
    const int Max = dimSize - 1;
    for(int i = 0; i < dimSize; i++){
        cin >> rating[i];
    }
    int Sum = 0;
    for (int i = 0; i < dimSize; i++){
         Sum += 500;
        if(i > Min && rating[i] > rating[i-1]){
            Sum += 500;
        }
        if(i < Max && rating[i] < rating[i+1]){
            Sum += 500;
        }
        Sum += rating[i];
    }
        
    cout << Sum;
    return 0;
}